## Attention! Project Status Information!

Since the TOLO integration has fully been merged into <a href="https://www.home-assistant.io/integrations/tolo/">Home Assistant</a>, this project has been archived.
Further development will take place in the [Home Assistant Core Repository](https://github.com/home-assistant/core).
For discussion, questions and feature requests please use the official [Home Assistant help channels](https://www.home-assistant.io/help/).

For technical questions and issues related to TOLO Sauna network based configuration, please check https://gitlab.com/MatthiasLohr/tololib.


# TOLO Sauna Home Assistant Integration

This Home Assistant integration allows to control your TOLO Sauna / Steam Bath using your Home Assistant instance.
Since this integration has not been added yet to the [Home Assistant Core](https://github.com/home-assistant/core)
(see [this ticket](https://github.com/home-assistant/core/pull/55619)),
this repository offers to install the TOLO Sauna integration as [custom component](#setup-instructions).

As soon as this integration has been officially added to Home Assistant,
this project will be archived.

## Supported Features

  * Power on/off
  * Set target temperature
  * Show current temperature and heating state (heating, idle, off)

For a full list of features (settings and status information) [see here](docs/feature-mappings.md).
Over time, more and more features will be added.
Feel free to support development by [contributing](CONTRIBUTING.md) to this project!

## Screenshots

| Control | Overview Screenshot | Details Screenshot |
| ------- | ------------------- | ------------------ |
| Climate | ![tolo-climate-overview](https://gitlab.com/MatthiasLohr/homeassistant-tolo/-/wikis/uploads/55139f78261067a0a5f0df94e6ceef2f/tolosauna-climate-overview.png) | ![tolosauna-climate-details](https://gitlab.com/MatthiasLohr/homeassistant-tolosauna/-/wikis/uploads/f78199429f18dfed3ce32a4b84a58acd/tolosauna-climate-details.png) |



## Setup Instructions

Since the TOLO Sauna integration is not an official part of Home Assistant yet,
some manual work is required to install the integration to your Home Assistant instance:

  * `cd` to your Home Assistant `config` directory.
  * If not there yet, create a folder `custom_components` in your `config` directory.
  * Download and extract the [latest release](https://gitlab.com/MatthiasLohr/homeassistant-tolo/-/releases) of this project to the `custom_components` directory.
    This way you should end up with a `config/custom_components/tolosauna` directory.
  * Restart your Home Assistant instance.
  * Log in to your Home Assistant instance and go to the integrations page.
    If you have a TOLO Sauna connected to your network, auto discovery should already offer to configure the integration.


## Acknowledgements

  * Thanks to [KAISER Wellness GmbH](https://www.dampfgenerator.at/) for supporting the development by providing the TOLO Sauna App Box (networking device)


## Legal Notice

This project is a community project, published under the [MIT License](LICENSE.md).
See [LICENSE.md](https://gitlab.com/MatthiasLohr/homeassistant-tolosauna/-/blob/main/LICENSE.md) for more information.

Neither this project, nor the author is in any professional affiliation with companies behind TOLO.

Copyright (c) by [Matthias Lohr](https://mlohr.com/) &lt;[mail@mlohr.com](mailto:mail@mlohr.com)&gt;
