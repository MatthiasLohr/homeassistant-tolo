# Changelog

## v1.1.0

  * Added lamp controls (light)


## v1.0.0

  * First release with basic climate control (sauna temperature and humidity, fan control)
